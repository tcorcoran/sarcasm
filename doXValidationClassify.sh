#!/bin/bash

data_dir=/home/thomas/sarcasm/

for i in 1 2 3 4 5 6 7 8 9 10
do
    /./home/thomas/svm/svm_light/svm_classify ${data_dir}TEST/test${i} ${data_dir}out/light_model${i} ${data_dir}out/light_prediction${i} > ${data_dir}out/light_XVAL_results_${i} & 
done
