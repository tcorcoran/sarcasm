import sklearn.datasets.svmlight_format as svmlight
from sklearn.feature_selection import chi2, SelectKBest
import sys
if len(sys.argv) < 4:
    print "arg1 == k, arg 2 == input file, arg3 == output file"
    print 'Stopping ...'
    exit()
k = int(sys.argv[1])
svm_light_in = sys.argv[2]
svm_light_out = sys.argv[3]
X,y = svmlight.load_svmlight_file(svm_light_in)

ch2 = SelectKBest(chi2, k=k)
X_out = ch2.fit_transform(X,y)

temp_file = "temp_file_po2km"
svmlight.dump_svmlight_file(X_out,y, temp_file)

with open(temp_file, 'rb') as fi:
    with open(svm_light_out, 'wb') as fo:
        for line in fi:
            q = line.split()
            if q[1][0] == '0':
                        #            set_trace()
                q.append(sys.argv[2]+':'+q[1].split(':')[1])
                q.remove(q[1])
                line = ' '.join(q)
                fo.write(line)

