import csv 
import nltk
import json
import cPickle
import itertools
from glob import glob
from ast import literal_eval
from os.path import splitext,split
from operator import itemgetter
from collections import defaultdict, Counter, OrderedDict
from pdb import set_trace
from nltk.stem.porter import PorterStemmer
from getFeatures import getLIWC, makeHeaderIndex
stemmer = PorterStemmer()

with open('/home/thomas/sarcasm/liwc_lookup.json', 'rb') as fi: 
        liwc_lu = json.load(fi)
with open('/home/thomas/sarcasm/liwc_dict.cPickle', 'rb') as fi: 
        liwc = cPickle.load(fi)

def getLabel(data_set_file):
    d = defaultdict(dict)
    if 'qr' in data_set_file:
        json_dir = '/home/thomas/qr_meta/json_files/'
    elif '123' in data_set_file:
        json_dir = '/home/thomas/123_jsons/'
                                    
    with open(data_set_file, 'rb') as fi: 
        reader = csv.reader(fi, delimiter='\t')
        reader.next() # Skip header line
        for row in reader:
            d[json_dir+row[0].replace(' ','')+'.json']['label'] = row[1]
            d[json_dir+row[0].replace(' ','')+'.json']['sentences'] = OrderedDict()
            
    return d

def getFeatures(keysANDcounts):
    q = 0 
    header = set()
    for post_file in keysANDcounts:
                        
        try:
            with open(post_file, 'rb') as fi: 
                post = json.load(fi)
        except IOError:
            print "Warning: cannot open file: %s\t...Bad Parse?"%post_file
    
        post_text= [[word.lower() for word in sentence['text']] for sentence in post['sentences']]
        post_POS = [['POS-'+word[1]['PartOfSpeech'] for word in sentence['words']] for sentence in post['sentences']]
        post_count = Counter()
        for i, (text, POS) in enumerate(zip(post_text,post_POS)):
            word_bigrams = nltk.util.ngrams(text, 2)
            word_trigrams = nltk.util.ngrams(text, 3)
            POS_bigrams = nltk.util.ngrams(POS, 2)
            POS_trigrams = nltk.util.ngrams(POS, 3)                                                                                   
            LIWC_feats = getLIWC(text)
            
            Total_Words = len(text)
            Total_Char = sum([len(word) for word in text])
            Ave_Chars_Per_Word = Total_Char / float(len(text))

            all_feats = text + POS + word_bigrams + word_trigrams + POS_bigrams + POS_trigrams + LIWC_feats
            
            count = Counter()
            
            for ngram in all_feats:
                header.add(ngram) # this turns out to be O(1) where x not in l is O(n) (makes a HUGE difference ~10 hrs)
                count[ngram] += 1
                post_count[ngram] += 1
            count['Total_Words'] = Total_Words
            count['Total_Char'] = Total_Char
            count['Ave_Chars_Per_Word'] = Ave_Chars_Per_Word
            
            keysANDcounts[post_file]['sentences'][i] = count
            #print i
        keysANDcounts[post_file]['post'] = post_count    
        q += 1
        print str(q) + '######'
    return keysANDcounts, header

one23 = getLabel('/home/thomas/sarcasm/all_123_balanced.csv')
qr = getLabel('/home/thomas/sarcasm/all_qr_balanced.csv')
keysANDcounts = one23.copy()
keysANDcounts.update(qr)
keysANDcounts, header = getFeatures(keysANDcounts)

header.add('Total_Words')
header.add('Total_Char')
header.add('Ave_Chars_Per_Word')

header = list(header)
header.insert(0,'Label')
header_index_table = makeHeaderIndex(header)

init = open('init.dat', 'wb')
with open('results.dat', 'wb') as fo: 
    set_trace()
    for k in keysANDcounts: # Write label: 1 == sarcastic, -1 == not sarcastic
        if keysANDcounts[k]['label'] == 'sarcastic':
            fo.write("1\t")
        else:
            fo.write('-1\t')
        num_sentences = len(keysANDcounts[k]['sentences'])            
        fo.write(str(num_sentences)+'\n')
        init.write(str(num_sentences)+'\t')
        init.write('\t'.join([str(x) for x in range(1, num_sentences+1)]) +'\n')

        for sentence_i in sorted(keysANDcounts[k]['sentences']):
            fo.write(str(sentence_i)+'\t')
            tuple_pairs = []
            for ngram in keysANDcounts[k]['sentences'][sentence_i]:
                i = header_index_table[ngram]
                tuple_pairs.append((i,keysANDcounts[k]['sentences'][sentence_i][ngram]))
                tuple_pairs.sort(key = itemgetter(0)) # svm-light only takes features with increasing  index values

            for pair in tuple_pairs:
                fo.write("%d:%d\t"%pair) 
            fo.write('\n')

#           tuple_pairs = []
#           for ngram in keysANDcounts[k]['post']:
#               I = header_index_table[ngram]
#               tuple_pairs.append((I,keysANDcounts[k]['post'][ngram]))
#
#           tuple_pairs.sort(key = itemgetter(0)) # svm-light only takes features with increasing  index values
#
#                                                                                            for pair in tuple_pairs:
#                                                                                                                fo.write("%d:%d\t"%pair)
                                                                                                                



init.close()
