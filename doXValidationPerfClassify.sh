#!/bin/bash

data_dir=/home/thomas/sarcasm/
c=$1

for i in 1 2 3 4 5 6 7 8 9 10
do
    /./home/thomas/svm/svm_perf/svm_perf_classify ${data_dir}TEST/test${i} ${data_dir}out/perf_modelCeq${c}-${i} ${data_dir}out/perf_predictionCeq${c}-${i} > ${data_dir}out/perf_XVAL_results_Ceq${c}-${i} & 

done
