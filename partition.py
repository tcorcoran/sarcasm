#!/usr/bin/python
import sys
from random import shuffle
from pdb import set_trace
post_count = 6258
samp_size = post_count/10

d = {'sample1':[], 'sample2':[], 'sample3':[], 'sample4':[], 'sample5':[]
     ,'sample5':[], 'sample6':[], 'sample7':[], 'sample8':[], 'sample9':[], 
     'sample10':[]}

order = range(1, post_count+1)
shuffle(order)

for s in d:
    while len(d[s]) < samp_size:
        d[s].append(order.pop())

lines_dict = {}
if sys.argv < 2:
    results_in = 'results.dat'
else:
    results_in = sys.argv[1]

with open('/home/thomas/sarcasm/'+results_in, 'rb') as fi:
    for i,line in enumerate(fi, start=1):
#        set_trace()
        q = line.split()
        if q[1][0] == '0':
#            set_trace()
            q.append(sys.argv[2]+':'+q[1].split(':')[1])
            q.remove(q[1])
        line = ' '.join(q)
        lines_dict[i] = line + '\n'

for dict_i in xrange(1,len(d)+1):
    print "#############sample: %d###################"%dict_i
#    set_trace()

    with open('TRAIN/train'+str(dict_i), 'wb') as train:
        with open('TEST/test' +str(dict_i), 'wb') as test:
            for line_i in xrange(1,1+post_count):
                # set_trace()
                if line_i not in d['sample'+str(dict_i)]:
                    train.write(lines_dict[line_i])
                else:
                    test.write(lines_dict[line_i])
                print "line: %d"%line_i
