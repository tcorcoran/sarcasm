#!/usr/bin/python
import re
import csv
import nltk
import json
import cPickle
import itertools
from glob import glob
from pdb import set_trace
from ast import literal_eval
from operator import itemgetter
from os.path import splitext,split
from nltk.stem.porter import PorterStemmer
from collections import defaultdict, Counter

stemmer = PorterStemmer()

with open('/home/thomas/sarcasm/liwc_lookup.json', 'rb') as fi:
    liwc_lu = json.load(fi)
with open('/home/thomas/sarcasm/liwc_dict.cPickle', 'rb') as fi:
    liwc = cPickle.load(fi)
sarc_cues = set()

# read in sarcasm cues
ngram_files = ['unigrams.csv','bigrams.csv','trigrams.csv']
for ngramFile in ngram_files:
    with open('/home/thomas/sarcasm/eacl_stuff/sarcasm_'+ngramFile, 'r') as fi:
        reader = csv.reader(fi,delimiter='\t')
        reader.next() # skip headers
        for line in reader:
            cue = line[0].strip()
            if cue: # sarc cues are messy--extra whitespace,quotes
                if cue[0] == "'":
                    cue = cue.strip("'")
                    sarc_cues.add("`" + cue + "'")
                    sarc_cues.add("``" + cue + '"')
                sarc_cues.add(cue)


def getLIWC(text):
    """Count liwc for text
    text should be a single list of tokens, lowercase
    NOTE: things here are not counted, unlike length and sarc cues
    what's returned is a list of LIWC-category_name strings, uncounted
    """
    results = []
    for token in text:
        for word in liwc_lu:
            if word[-1] == '*': # stem of word
                if token.startswith(word):
                    for ID in liwc_lu[word]:
                        results.append('LIWC-'+liwc[ID]['cat_name'])
            else: # exact word match
                if token == word:
                    for ID in liwc_lu[word]:
                        results.append('LIWC-'+liwc[ID]['cat_name'])
    return results

def getLength(text_flat, sentences):
    """finds 5 different measures of length for a post
    NOTE: the keys here must be added to the header later 
    because of the way lenth features are added to the count
    """
    d = Counter()
    d['LENGTH-total_words'] = len(text_flat)
    d['LENGTH-total_char'] = sum([len(word) for word in text_flat])
    d['LENGTH-total_sentences'] = len(sentences)
    d['LENGTH-ave_words_per_sent'] = len(text_flat) / float(len(sentences))
    d['LENGTH-ave_chars_per_word'] = d['LENGTH-total_char'] / float(len(text_flat))
    return d

scue_debug = set() 
def getSarcCues(joined_text):
    """
    joined_text should be a join on whitespace of tokenized text

    """
    cnt = Counter()
    for ngram in sarc_cues:
        pat = re.compile(r'\b'+re.escape(ngram)+r'\b')
        found = pat.findall(joined_text)
        cnt[ngram] += len(found)
        if len(found) > 0:# debugging, some ngrams were missing
            scue_debug.add(ngram)
    return cnt

def getInterjections():
    pass

def getDiscorseCue():
    pass


def getLabel(data_set_file, unbalanced=False):
    """Use label from data_set_file
       dict keys are file paths because keys from the two
       data sets overlap :(
    """
    d = defaultdict(dict)
    if unbalanced:
        # use for unbalanced an data set
        with open('/home/thomas/sarcasm/123_results_nodups.csv') as fi:
            reader = csv.DictReader(fi, delimiter='\t')
            for row in reader:
                if int(row['sarcasm_yes_count']) >= 2:
                    label = 'sarcastic'
                else:
                    label = 'not sarcastic'
                d["/home/thomas/123_jsons/(%s,%s).json"%(row['page_id'],row['question_set_id'])]['label'] = label
        return d
    if 'qr' in data_set_file:
        json_dir = '/home/thomas/qr_meta/json_files/'
    elif '123' in data_set_file:
        json_dir = '/home/thomas/123_jsons/'
    
    
    with open(data_set_file, 'rb') as fi:
        reader = csv.reader(fi, delimiter='\t')
        reader.next() # Skip header line
        for row in reader:
            d[json_dir+row[0].replace(' ','')+'.json']['label'] = row[1]

    return d

def getFeatures(keysANDcounts):
    q = 0
    header = set()
    for post_file in keysANDcounts:
               
        try:
            with open(post_file, 'rb') as fi:
                post = json.load(fi)
        except IOError:
            print "Warning: cannot open file: %s\t...Bad Parse?"%post_file
        
#        post_text = [[stemmer.stem_word(word[1]['Lemma']).lower() for word in sentence['words']] for sentence in post['sentences']]      # stemmed text--not used
        post_text= [[word.lower() for word in sentence['text']] for sentence in post['sentences']]

        post_POS = [['POS-'+word[1]['PartOfSpeech'] for word in sentence['words']] for sentence in post['sentences']]
        

        # find the ngrams here.
        # these are nested lists...
        POS_bigrams = [nltk.util.ngrams(text, 2) for text in post_POS]
        POS_trigrams =  [nltk.util.ngrams(text, 3) for text in post_POS]
        # so are these
        word_bigrams = [nltk.util.ngrams(text, 2) for text in post_text]
        word_trigrams = [nltk.util.ngrams(text, 3) for text in post_text]
        
        # unnest the lists here
        pos_unigrams = list(itertools.chain.from_iterable(post_POS))
        pos_bigrams = list(itertools.chain.from_iterable(POS_bigrams))
        pos_trigrams = list(itertools.chain.from_iterable(POS_trigrams))

        word_unigrams = list(itertools.chain.from_iterable(post_text))
        word_bigrams = list(itertools.chain.from_iterable(word_bigrams))
        word_trigrams = list(itertools.chain.from_iterable(word_trigrams))
        
        # get liwc features
        liwc_feats = getLIWC(word_unigrams)

        # merge 1 2 3 grams
        # we can change what's in ngrams to get different feature files
        ngrams =  word_unigrams  + word_bigrams + word_trigrams + liwc_feats + pos_bigrams + word_trigrams + pos_trigrams + pos_unigrams
        
        # count up all the unique ngrams in this post
        count = Counter()
        for ngram in ngrams:
            header.add(ngram) # add to header
            count[ngram] += 1 
        
        # add the length features for this post to the count
        count += getLength(word_unigrams, post_text)
        
        # This is the counter for the sarcasm cues
        # comment out the above counter to use this and get sarcasm cues only
#        sarc_cue_counts = getSarcCues(' '.join(word_unigrams))
#        count += sarc_cue_counts
#        for ngram in sarc_cue_counts: 
#            header.add(ngram)

#        count['SARC_CUE_WORDS'] = sarc_cue_counts # not used in any experiment

        keysANDcounts[post_file]['ngrams_count'] = count
        q+=1
        print q
    return keysANDcounts, header

def makeHeaderIndex(header):
    """match each entry in header to a unique ID,
    as required by SVM-light
    I.e each feature has a unique ID 
    """
    header_index_table = {}
    with open('Feature-Index.dat', 'wb') as fo: 
        for i,h in enumerate(header):
            header_index_table[h] = i # assign unique index for each feature

            if isinstance(h, tuple):
                try: fo.write("%d:\"\"\"%s\"\"\"\t"% \
                        (i, ' '.join([x.encode('utf8') for x in h]))) 
                except: set_trace()
            else:
                try: fo.write("%d:%s\t"%(i, h.encode('utf8')))
                except: set_trace()
    return header_index_table


if __name__ == '__main__':
    
    # dict of the unique post IDs with a Counter() object
    # as the value of each ID. feature counts are stored in this Counter
    one23 = getLabel('/home/thomas/sarcasm/all_123_balanced.csv')
    qr = getLabel('/home/thomas/sarcasm/all_qr_balanced.csv')
    keysANDcounts = one23.copy()
    keysANDcounts.update(qr) 

    #keysANDcounts = getLabel(keysANDcounts)
    keysANDcounts, header = getFeatures(keysANDcounts) # collect all features 

    # we have to add these manually because of the way the length is counted
    # I suppose I could have getLenth try to add these keys to the header each time
    # it's calle
    header.add('LENGTH-total_words')
    header.add('LENGTH-total_char')
    header.add('LENGTH-total_sentences')
    header.add('LENGTH-ave_words_per_sent')
    header.add('LENGTH-ave_chars_per_word')
#    header.add('SARC_CUE_WORDS')
   
    header = list(header)
    header.insert(0,'Label')
    header_index_table = makeHeaderIndex(header)

    with open('results.dat', 'wb') as fo:
        for k in keysANDcounts: # Write label: 1 == sarcastic, -1 == not sarcastic
            if keysANDcounts[k]['label'] == 'sarcastic':
                fo.write("1\t")
            else:
                fo.write('-1\t')
            
            try: assert k in keysANDcounts
            except:
                # These files have no text/POS or something else bad happened
                print "Warning: Bad parse in file: %s.json"%str(k) 
            
            tuple_pairs = []
            for ngram in keysANDcounts[k]['ngrams_count']:
                i = header_index_table[ngram]
                tuple_pairs.append((i,keysANDcounts[k]['ngrams_count'][ngram]))

            tuple_pairs.sort(key = itemgetter(0)) # svm-light only takes features with increasing  index values

            for pair in tuple_pairs:
                fo.write("%d:%d\t"%pair)
            
            fo.write('\n')
    print "Total number of features: %d"%len(header)
    print "Total number of posts: %d"%len(keysANDcounts)
