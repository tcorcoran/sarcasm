#!/bin/bash

data_dir="/home/thomas/sarcasm/"
c=$1
for i in 1 2 3 4 5 6 7 8 9 10 
do
    /./home/thomas/svm/svm_perf/svm_perf_learn -c ${c}   ${data_dir}TRAIN/train${i} ${data_dir}out/perf_modelCeq${c}-${i} > ${data_dir}out/perf_learnCeq${c}-${i} & 
    echo $i
done

